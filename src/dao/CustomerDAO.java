package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


import connection.ConnectionFactory;
import model.Customer;



public class CustomerDAO {

	protected static final Logger LOGGER = Logger.getLogger(CustomerDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO customer (name,address,email,age)"
			+ " VALUES (?,?,?,?)";
	private final static String findStatementString = "SELECT * FROM customer where cid = ?";
	private final static String findStatementString2= "SELECT * FROM customer where name = ?";
	private final static String deleteStatementString= "DELETE FROM customer where cid = ?";
	private final static String findallStatementString= "SELECT * FROM customer ";
	/**
	 * 
	 * @param cid id to be found
	 * @return result of the find the querry
	 */
	public static Customer findById(int cid) {
		Customer toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, cid);
			rs = findStatement.executeQuery();
			rs.next();

			String name = rs.getString("name");
			String address = rs.getString("address");
			String email = rs.getString("email");
			int age = rs.getInt("age");
			toReturn = new Customer(cid, name, address, email, age);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"CustomerDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	/**
	 * 
	 * @param name name to be found
	 * @return result of the find query based on name
	 */
	public static Customer findByName(String name) {
		Customer toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString2);
			findStatement.setString(1, name);
			rs = findStatement.executeQuery();
			rs.next();

			int cid  = rs.getInt("cid");
			String address = rs.getString("address");
			String email = rs.getString("email");
			int age = rs.getInt("age");
			toReturn = new Customer(cid, name, address, email, age);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"CustomerDAO:findByName " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	/**
	 * 
	 * @param cust Customer to be inserted
	 * @return result of the insert query the inserted id
	 */
	public static int insert(Customer cust) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, cust.getName());
			insertStatement.setString(2, cust.getAddress());
			insertStatement.setString(3, cust.getEmail());
			insertStatement.setInt(4, cust.getAge());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "CustomerDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
	/**
	 * 
	 * @param cid id to which the deletion is performed on
	 */
	public static void delete(int cid) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement deleteStatement=null;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString, Statement.RETURN_GENERATED_KEYS);
			deleteStatement.setInt(1, cid);
			deleteStatement.executeUpdate();

		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "CustomerDAO:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
	public static ArrayList<Customer> getAllcustomers()
	{
		ArrayList<Customer> allcustomers=new ArrayList<Customer>();
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findallStatementString);
			rs = findStatement.executeQuery();
			try
			{
			while (rs.next())
			{
				String name = rs.getString("name");
				String address = rs.getString("address");
				String email = rs.getString("email");
				int age = rs.getInt("age");
				int cid=rs.getInt("cid");
				allcustomers.add(new Customer(cid,name,address,email,age));
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"CustomerDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		}
		catch (SQLException e) {
			e.printStackTrace();}
		return allcustomers;
	}
	
	
}
