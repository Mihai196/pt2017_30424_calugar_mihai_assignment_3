package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


import connection.ConnectionFactory;
import model.Product;



public class ProductDAO {

	protected static final Logger LOGGER = Logger.getLogger(ProductDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO product (price,quantity,name)"
			+ " VALUES (?,?,?)";
	private final static String findStatementString = "SELECT * FROM product where pid= ?";
	private final static String deleteStatementString= "DELETE FROM product where pid = ?";
	private final static String findStatementString2= "SELECT * FROM product where name = ?";
	private final static String findallStatementString= "SELECT * FROM product ";

	/**
	 * 
	 * @param pid
	 * @return result of the find query for id 
	 */
	public static Product findById(int pid) {
		Product toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, pid);
			rs = findStatement.executeQuery();
			rs.next();

			String name = rs.getString("name");
			int price = rs.getInt("price");
			int quantity=rs.getInt("quantity");
			toReturn = new Product(pid,  price, quantity, name);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProductDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	/**
	 * 
	 * @param name
	 * @return result of the find query by name
	 */
	public static Product findByName(String name) {
		Product toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString2);
			findStatement.setString(1, name);
			rs = findStatement.executeQuery();
			rs.next();

			int pid=rs.getInt("pid");
			int price = rs.getInt("price");
			int quantity=rs.getInt("quantity");
			toReturn = new Product(pid,  price, quantity, name);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProductDAO:findByName " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	/**
	 * 
	 * @param prod product to be inserted
	 * @return result of the insert query - id of the newly inserted product
	 */
	public static int insert(Product prod) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, prod.getPrice());
			insertStatement.setInt(2, prod.getQuantity());
			insertStatement.setString(3, prod.getName());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
	/**
	 * 
	 * @param pid id on which the deletion is performed on
	 */
	public static void delete(int pid) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement deleteStatement=null;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString, Statement.RETURN_GENERATED_KEYS);
			deleteStatement.setInt(1, pid);
			deleteStatement.executeUpdate();

		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDAO:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
	public static ArrayList<Product> getAllProducts()
	{
		ArrayList<Product> allproducts=new ArrayList<Product>();
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findallStatementString);
			rs = findStatement.executeQuery();
			try
			{
			while (rs.next())
			{
				String name = rs.getString("name");
				int quantity = rs.getInt("quantity");
				int pid=rs.getInt("pid");
				int price=rs.getInt("price");
				allproducts.add(new Product(pid,price,quantity,name));
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"CustomerDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		}
		catch (SQLException e) {
			e.printStackTrace();}
		return allproducts;
	}
	
}
