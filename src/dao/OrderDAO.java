package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import model.Customer;
import model.Order;

public class OrderDAO {
	protected static final Logger LOGGER = Logger.getLogger(OrderDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO `order` (totPrice,cid)"
			+ " VALUES (?,?)";
	private final static String findStatementString = "SELECT * FROM `order` where oid = ?";
	private final static String deleteStatementString= "DELETE FROM `order` where oid = ?";
	private final static String findallStatementString= "SELECT * FROM `order`";
	
	/**
	 * 
	 * @param oid 
	 * @return results of the find query for orderd id
	 */
	public static Order findById(int oid) {
		Order toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, oid);
			rs = findStatement.executeQuery();
			rs.next();
			int totPrice=rs.getInt("totPrice");
			int cid = rs.getInt("cid");
			toReturn = new Order(oid, totPrice,cid);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"OrderDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	/**
	 * 
	 * @param ord the order to be inserted
	 * @return result of the insert query mainly the inserted id
	 */
	public static int insert(Order ord) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, ord.getTotPrice());
			insertStatement.setInt(2, ord.getCid());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "OrderDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
	/**
	 * 
	 * @param oid id on which the deletion is performed on
	 */
	public static void delete(int oid) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement deleteStatement=null;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString, Statement.RETURN_GENERATED_KEYS);
			deleteStatement.setInt(1, oid);
			deleteStatement.executeUpdate();

		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "OrderDAO:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
	public static ArrayList<Order> getAllOrders()
	{
		ArrayList<Order> allorders=new ArrayList<Order>();
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findallStatementString);
			rs = findStatement.executeQuery();
			try
			{
			while (rs.next())
			{
				int oid=rs.getInt("oid");
				int cid=rs.getInt("cid");
				int totPrice=rs.getInt("totPrice");
				allorders.add(new Order(oid,totPrice,cid));
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"OrderDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		}
		catch (SQLException e) {
			e.printStackTrace();}
		return allorders;
	}

}
