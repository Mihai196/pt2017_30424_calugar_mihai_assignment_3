package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import model.OrderDetails;

public class OrderDetailsDAO {
	protected static final Logger LOGGER = Logger.getLogger(OrderDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO orderdetails (amount,oid,pid)"
			+ " VALUES (?,?,?)";
	private final static String findStatementString = "SELECT * FROM orderdetails where detailid = ?";
	private final static String deleteStatementString= "DELETE FROM orderdetails where detailid = ?";
	private final static String findallStatementString= "SELECT * FROM orderdetails ";
	
	/**
	 * 
	 * @param detailid
	 * @return the result of the find query by id 
	 */
	public static OrderDetails findById(int detailid) {
		OrderDetails toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, detailid);
			rs = findStatement.executeQuery();
			rs.next();
			int amount=rs.getInt("amount");
			int oid = rs.getInt("oid");
			int pid=rs.getInt("pid");
			toReturn = new OrderDetails(detailid, amount,oid,pid);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"OrderDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	/**
	 * 
	 * @param ord
	 * @return the id of the newly inserted orderdetails
	 */
	public static int insert(OrderDetails ord) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, ord.getAmount());
			insertStatement.setInt(2, ord.getOid());
			insertStatement.setInt(3, ord.getPid());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "OrderDetailsDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
	/**
	 * 
	 * @param detailid on which the deletion is performed on 
	 */
	public static void delete(int detailid) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement deleteStatement=null;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString, Statement.RETURN_GENERATED_KEYS);
			deleteStatement.setInt(1, detailid);
			deleteStatement.executeUpdate();

		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "OrderDetailsDAO:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
	public static ArrayList<OrderDetails> getAllOrders()
	{
		ArrayList<OrderDetails> allorders=new ArrayList<OrderDetails>();
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findallStatementString);
			rs = findStatement.executeQuery();
			try
			{
			while (rs.next())
			{
				int oid=rs.getInt("oid");
				int pid=rs.getInt("pid");
				int amount=rs.getInt("amount");
				int detailid=rs.getInt("detailid");
				allorders.add(new OrderDetails(detailid,amount,oid,pid));
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"OrderDetailsDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		}
		catch (SQLException e) {
			e.printStackTrace();}
		return allorders;
	}


}
