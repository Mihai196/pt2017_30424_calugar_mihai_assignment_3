package GUI;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import blogic.CustomerBLL;
import model.Customer;


public class View {
	public JFrame frame;
	private JFrame frame2;
	private JFrame frame3;
	private JFrame frame4;
	private JFrame frame5;
	private JFrame frame6;
	private JFrame frame7;
	private JFrame frame8;
	private JFrame frame9;
	private JTable customerTable=new JTable();
	private JTextField cid=new JTextField();
	private JTextArea foundcustomer;
	
	private JTextField name=new JTextField();
	private JTextField address=new JTextField();
	private JTextField email=new JTextField();
	private JTextField age=new JTextField();
	
	private JTextField idtoDelete=new JTextField();
	private JTextField nametoFind=new JTextField();
	private JTextArea foundcustname=new JTextArea();
	
	private JTextField oid=new JTextField();
	private JTextArea foundorder;
	
	private JTextField totPricetoinsert=new JTextField();
	private JTextField cidtoInsert=new JTextField();
	private JTable ordTable=new JTable(); 
	private JTextField oidtoDelete=new JTextField();
	
	private JTextField did=new JTextField();
	private JTextArea founddetail;
	private JTextField amounttoInsert=new JTextField();
	private JTextField doidtoInsert=new JTextField();
	private JTextField pidtoInsert=new JTextField();
	private JTable ordDetailsTable=new JTable();
	private JTextField didtoDelete=new JTextField();
	
	private JTextField pid=new JTextField();
	private JTextArea foundproduct;
	
	private JTextField price=new JTextField();
	private JTextField quantity=new JTextField();
	private JTextField nameprod=new JTextField();
	private JTable prodTable=new JTable();
	private JTextField pidtoDelete=new JTextField();
	private JTextField nameproduct=new JTextField();
	private JTextArea foundprodname;
	
	private JTextField idtobill=new JTextField();
	private JTextArea bill;
	
	private Controller c1;
	private Controller c2;
	private Controller c3;
	private Controller c4;
	private Controller c5;
	private Controller c6;
	private Controller c7;
	private Controller c8;
	private Controller c9;
	private Controller c10;
	private Controller c11;
	private Controller c12;
	private Controller c13;
	private Controller c14;
	private Controller c15;
	private Controller c16;
	private Controller c17;
	private Controller c18;
	private Controller c19;
	public View ()
	{
		initframe();
		addOpsonCustomers();
		addOpsonOrders();
		addOpsonOrderDetails();
		addOpsonProducts();
		c1=new Controller(cid,foundcustomer,1);
		c2=new Controller(name,address,email,age);
		c3=new Controller(customerTable,1);
		c4=new Controller(idtoDelete,1);
		c5=new Controller(oid,foundorder,2);
		c6=new Controller(totPricetoinsert,cidtoInsert);
		c7=new Controller(ordTable,2);
		c8=new Controller(oidtoDelete,2);
		c9=new Controller(did,founddetail,3);
		c10=new Controller(amounttoInsert,doidtoInsert,pidtoInsert,1);
		c11=new Controller(ordDetailsTable,3);
		c12=new Controller(didtoDelete,3);
		c13=new Controller(nametoFind,foundcustname,4);
		c14=new Controller(pid,foundproduct,5);
		c15=new Controller(price,quantity,nameprod,2);
		c16=new Controller(prodTable,4);
		c17=new Controller(pidtoDelete,4);
		c18=new Controller(nameproduct,foundprodname,6);
		c19=new Controller(idtobill,bill,7);
	}
	public void initframe()
	{
		frame = new JFrame("Warehouse Administration");
		frame.setBounds(10, 10, 820, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
	}
	/**
	 * Adds ops on customers and action listeners for every button
	 */
	public void addOpsonCustomers()
	{
		JLabel ops=new JLabel("Ops on customers");
		ops.setBounds(50,10,150,20);
		ops.setFont(new Font("Times New Roman",Font.BOLD,14));
		frame.add(ops);
		
		
		cid.setBounds(120,40,20,20);
		frame.add(cid);
		
		foundcustomer=new JTextArea(1,30);
		JScrollPane scrollPanel1=new JScrollPane(foundcustomer);
		scrollPanel1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPanel1.setBounds(10,80,150,35);
		frame.add(scrollPanel1);
		
		JButton btn1 = new JButton("FindbyId:");
		btn1.setBounds(20, 40, 90, 20);
		frame.add(btn1);
		btn1.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				c1.findById();	
			}
		});
		JButton btn2 = new JButton("Insert");
		btn2.setBounds(20,150,70,20);
		frame.add(btn2);
		btn2.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				frame2 = new JFrame("Inserting");
				frame2.setBounds(10, 10, 400, 250);
				frame2.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				frame2.getContentPane().setLayout(null);
				frame2.setVisible(true);
				JLabel label1=new JLabel("Name");
				label1.setBounds(40,10,70,20);
				frame2.add(label1);
				name.setBounds(100,10,100,20);
				frame2.add(name);
				address.setBounds(100,40,100,20);
				JLabel label2=new JLabel("Address");
				label2.setBounds(40,40,70,20);
				frame2.add(label2);
				frame2.add(address);
				JLabel label3=new JLabel("Email");
				label3.setBounds(40,70,70,20);
				frame2.add(label3);
				email.setBounds(100,70,100,20);
				frame2.add(email);
				JLabel label4=new JLabel("Age");
				label4.setBounds(40,100,70,20);
				frame2.add(label4);
				age.setBounds(100,100,100,20);
				frame2.add(age);
				JButton btn3 = new JButton("Insert");
				btn3.setBounds(100,150,70,20);
				frame2.add(btn3);
				btn3.addActionListener(new ActionListener(){

					@Override
					public void actionPerformed(ActionEvent e) {
						c2.insertCustomer();
						
					}
					
				});
				
			}
			
		});
		JButton btn4 = new JButton("SeeCustomers");
		btn4.setBounds(20,175,150,20);
		frame.add(btn4);
		btn4.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				frame3 = new JFrame("Inserting");
				frame3.setBounds(10, 10, 500, 250);
				frame3.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				frame3.getContentPane().setLayout(null);
				frame3.setVisible(true);
				JScrollPane scrollPanel2=new JScrollPane(customerTable);
				scrollPanel2.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
				scrollPanel2.setBounds(10,10,450,230);
				c3.printCustomers();
				frame3.add(scrollPanel2);
			
				
			}
			
		});
		
		JButton btn5 = new JButton("DeletebyId");
		btn5.setBounds(20,205,100,20);
		frame.add(btn5);
		idtoDelete.setBounds(123,205,30,20);
		frame.add(idtoDelete);
		btn5.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				c4.deleteCustomer();
			}
			
		});
		JButton btn6 = new JButton("FindbyName");
		btn6.setBounds(20,245,150,20);
		frame.add(btn6);
		nametoFind.setBounds(20,275,150,20);
		frame.add(nametoFind);
		foundcustname=new JTextArea(1,30);
		JScrollPane scrollPanel2=new JScrollPane(foundcustname);
		scrollPanel2.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPanel2.setBounds(20,310,150,35);
		frame.add(scrollPanel2);
		btn6.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				c13.findCustbyName();
			}
			
		});
		
		
	}
	/**
	 * Adds ops on orders and action listeners for the buttons corresponding to orders
	 */
	public void addOpsonOrders()
	{
		JLabel ops1=new JLabel("Ops on orders");
		ops1.setBounds(250,10,150,20);
		ops1.setFont(new Font("Times New Roman",Font.BOLD,14));
		frame.add(ops1);
		
		
		JButton btn1 = new JButton("FindbyId:");
		btn1.setBounds(220, 40, 90, 20);
		frame.add(btn1);
		oid.setBounds(320,40,20,20);
		frame.add(oid);
		foundorder=new JTextArea(1,30);
		JScrollPane scrollPanel1=new JScrollPane(foundorder);
		scrollPanel1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPanel1.setBounds(220,80,150,35);
		frame.add(scrollPanel1);
		btn1.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				c5.findorderByID();
			}
		});
		JButton btn2 = new JButton("Insert");
		btn2.setBounds(220,150,70,20);
		frame.add(btn2);
		btn2.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				frame4 = new JFrame("Inserting");
				frame4.setBounds(10, 10, 400, 250);
				frame4.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				frame4.getContentPane().setLayout(null);
				frame4.setVisible(true);
				JLabel label3=new JLabel("TotalPrice");
				label3.setBounds(40,10,70,20);
				frame4.add(label3);
				totPricetoinsert.setBounds(100,10,100,20);
				frame4.add(totPricetoinsert);
				JLabel label4=new JLabel("Cid");
				label4.setBounds(40,40,70,20);
				frame4.add(label4);
				cidtoInsert.setBounds(100,40,100,20);
				frame4.add(cidtoInsert);
				JButton btn3 = new JButton("Insert");
				btn3.setBounds(100,150,70,20);
				frame4.add(btn3);
				btn3.addActionListener(new ActionListener(){

					@Override
					public void actionPerformed(ActionEvent e) {
						c6.insertOrder();
						
					}
					
				});
				
			}
			
		});
		JButton btn4 = new JButton("SeeOrders");
		btn4.setBounds(220,175,150,20);
		frame.add(btn4);
		btn4.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				frame5 = new JFrame("Inserting");
				frame5.setBounds(10, 10, 400, 250);
				frame5.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				frame5.getContentPane().setLayout(null);
				frame5.setVisible(true);
				JScrollPane scrollPanel2=new JScrollPane(ordTable);
				scrollPanel2.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
				scrollPanel2.setBounds(10,10,350,180);
				c7.printOrders();
				
				frame5.add(scrollPanel2);
			
				
			}
			
		});
		JButton btn5 = new JButton("DeletebyId");
		btn5.setBounds(220,205,100,20);
		frame.add(btn5);
		oidtoDelete.setBounds(340,205,30,20);
		frame.add(oidtoDelete);
		btn5.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				c8.deleteOrder();
			}
			
		});
		
		JButton btn6=new JButton("Make Bill Based on Id: ");
		btn6.setBounds(300,240,200,20);
		frame.add(btn6);
		idtobill.setBounds(520,240,50,20);
		frame.add(idtobill);
		
		bill=new JTextArea(1,30);
		JScrollPane scrollPanel2=new JScrollPane(bill);
		scrollPanel2.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPanel2.setBounds(230,280,350,180);
		frame.add(scrollPanel2);
		btn6.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				c19.makeBill();
			}
			
		});
		
		
		
	}
	/**
	 * Adds ops on order details and action listeners for buttons corresponding to order details
	 */
	public void addOpsonOrderDetails()
	{
		JLabel ops2=new JLabel("Ops on orderdetails");
		ops2.setBounds(450,10,150,20);
		ops2.setFont(new Font("Times New Roman",Font.BOLD,14));
		frame.add(ops2);
		JButton btn1 = new JButton("FindbyId:");
		btn1.setBounds(430, 40, 90, 20);
		frame.add(btn1);
		did.setBounds(530,40,20,20);
		frame.add(did);
		founddetail=new JTextArea(1,30);
		JScrollPane scrollPanel1=new JScrollPane(founddetail);
		scrollPanel1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPanel1.setBounds(430,80,150,35);
		frame.add(scrollPanel1);
		btn1.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				c9.findDetailById();
			}
		});
		JButton btn2 = new JButton("Insert");
		btn2.setBounds(430,150,70,20);
		frame.add(btn2);
		btn2.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				frame6 = new JFrame("Inserting");
				frame6.setBounds(10, 10, 400, 250);
				frame6.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				frame6.getContentPane().setLayout(null);
				frame6.setVisible(true);
				JLabel label3=new JLabel("Amount");
				label3.setBounds(40,10,70,20);
				frame6.add(label3);
				amounttoInsert.setBounds(100,10,100,20);
				frame6.add(amounttoInsert);
				JLabel label4=new JLabel("Oid");
				label4.setBounds(40,40,70,20);
				frame6.add(label4);
				doidtoInsert.setBounds(100,40,100,20);
				frame6.add(doidtoInsert);
				JLabel label5=new JLabel("Pid");
				label5.setBounds(40,70,70,20);
				frame6.add(label5);
				pidtoInsert.setBounds(100,70,100,20);
				frame6.add(pidtoInsert);
				
				JButton btn3 = new JButton("Insert");
				btn3.setBounds(100,150,70,20);
				frame6.add(btn3);
				btn3.addActionListener(new ActionListener(){

					@Override
					public void actionPerformed(ActionEvent e) {
						c10.insertOrderDetail();
						
					}
					
				});
				
			}
			
		});
		JButton btn4 = new JButton("SeeOrderDetails");
		btn4.setBounds(430,175,150,20);
		frame.add(btn4);
		btn4.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				frame7 = new JFrame("Inserting");
				frame7.setBounds(10, 10, 500, 250);
				frame7.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				frame7.getContentPane().setLayout(null);
				frame7.setVisible(true);
				//ordDetailsTable.setBounds(10,10,300,400);
				JScrollPane scrollPanel2=new JScrollPane(ordDetailsTable);
				scrollPanel2.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
				scrollPanel2.setBounds(10,10,450,230);
				c11.printOrderDetails();
				frame7.add(scrollPanel2);
			
				
			}
			
		});
		JButton btn5 = new JButton("DeletebyId");
		btn5.setBounds(430,205,100,20);
		frame.add(btn5);
		didtoDelete.setBounds(550,205,30,20);
		frame.add(didtoDelete);
		btn5.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				c12.deleteOrderDetail();
			}
			
		});
		
	}
	/**
	 * Adds ops on products and action listeners for buttons corresponding to Products 
	 */
	public void addOpsonProducts()
	{
		JLabel ops3=new JLabel("Ops on products");
		ops3.setBounds(650,10,150,20);
		ops3.setFont(new Font("Times New Roman",Font.BOLD,14));
		frame.add(ops3);
		
		JButton btn1 = new JButton("FindbyId:");
		btn1.setBounds(630, 40, 90, 20);
		frame.add(btn1);
		
		pid.setBounds(720,40,20,20);
		frame.add(pid);
		
		foundproduct=new JTextArea(1,30);
		JScrollPane scrollPanel1=new JScrollPane(foundproduct);
		scrollPanel1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPanel1.setBounds(630,80,150,35);
		frame.add(scrollPanel1);
		btn1.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				c14.findProductbyId();
			}
		});
		
		JButton btn2 = new JButton("Insert");
		btn2.setBounds(630,150,70,20);
		frame.add(btn2);
		btn2.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				frame8 = new JFrame("Inserting");
				frame8.setBounds(10, 10, 400, 250);
				frame8.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				frame8.getContentPane().setLayout(null);
				frame8.setVisible(true);
				JLabel label3=new JLabel("Price");
				label3.setBounds(40,10,70,20);
				frame8.add(label3);
				price.setBounds(100,10,100,20);
				frame8.add(price);
				JLabel label4=new JLabel("Quantity");
				label4.setBounds(40,40,70,20);
				frame8.add(label4);
				quantity.setBounds(100,40,100,20);
				frame8.add(quantity);
				JLabel label5=new JLabel("Name");
				label5.setBounds(40,70,70,20);
				frame8.add(label5);
				nameprod.setBounds(100,70,100,20);
				frame8.add(nameprod);
				
				JButton btn3 = new JButton("Insert");
				btn3.setBounds(100,150,70,20);
				frame8.add(btn3);
				btn3.addActionListener(new ActionListener(){

					@Override
					public void actionPerformed(ActionEvent e) {
						c15.insertProduct();
						
					}
					
				});
				
			}
			
		});
		JButton btn4 = new JButton("SeeProducts");
		btn4.setBounds(630,175,150,20);
		frame.add(btn4);
		btn4.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				frame9 = new JFrame("Inserting");
				frame9.setBounds(10, 10, 500, 250);
				frame9.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				frame9.getContentPane().setLayout(null);
				frame9.setVisible(true);
				//prodTable.setBounds(10,10,300,400);
				JScrollPane scrollPanel2=new JScrollPane(prodTable);
				scrollPanel2.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
				scrollPanel2.setBounds(10,10,450,230);
				c16.printProducts();
				frame9.add(scrollPanel2);
			}
		});
		JButton btn5 = new JButton("DeletebyId");
		btn5.setBounds(630,205,100,20);
		frame.add(btn5);
		pidtoDelete.setBounds(750,205,30,20);
		frame.add(pidtoDelete);
		btn5.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				c17.deleteProduct();
			}
			
		});
		JButton btn6 = new JButton("FindbyName");
		btn6.setBounds(630,245,150,20);
		frame.add(btn6);
		
		nameproduct.setBounds(630,275,150,20);
		frame.add(nameproduct);
		
		foundprodname=new JTextArea(1,30);
		JScrollPane scrollPanel2=new JScrollPane(foundprodname);
		scrollPanel2.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPanel2.setBounds(630,310,150,35);
		frame.add(scrollPanel2);
		btn6.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				c18.findProdbyName();
			}
			
		});
		

	}
	
}
