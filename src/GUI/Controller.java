package GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import blogic.CustomerBLL;
import blogic.OrderBLL;
import blogic.OrderDetailsBLL;
import blogic.ProductBLL;
import model.Customer;
import model.Order;
import model.OrderDetails;
import model.Product;


public class Controller {
	CustomerBLL custbll=new CustomerBLL();
	OrderBLL ordbll=new OrderBLL();
	OrderDetailsBLL orddbll=new OrderDetailsBLL();
	ProductBLL prdbll=new ProductBLL();
	
	private JTextField idtofind;
	private JTextArea foundcustomer;
	
	private JTextField nametoInsert;
	private JTextField addresstoInsert;
	private JTextField emailtoInsert;
	private JTextField agetoInsert;
	private JTextField idtoDelete;
	private JTextField nametoFind;
	private JTextArea foundcustname;
	
	private JTextField oid;
	private JTextArea foundorder;
	private JTextField totPricetoInsert;
	private JTextField cidtoInsert;
	private JTable custTable;
	private JTable ordTable;
	private JTextField oidtoDelete;
	
	private JTextField did;
	private JTextArea founddetail;
	private JTextField amount;
	private JTextField oidtoInsert;
	private JTextField pidtoInsert;
	private JTable orddetailsTable;
	private JTextField didtoDelete;
	
	private JTextField pid;
	private JTextArea foundproduct;
	private JTextField price;
	private JTextField quantity;
	private JTextField name;
	private JTable prodTable;
	private JTextField pidtoDelete;
	private JTextField prodnametoFind;
	private JTextArea foundprodname;
	private JTextField idtobill;
	private JTextArea bill;
	
	public Controller(JTextField name, JTextField address, JTextField email, JTextField age) {
		super();
		this.nametoInsert = name;
		this.addresstoInsert = address;
		this.emailtoInsert = email;
		this.agetoInsert = age;
	}
	public Controller(JTextField totPrice,JTextField cid)
	{
		this.totPricetoInsert=totPrice;
		this.cidtoInsert=cid;
	}
	public Controller(JTextArea bill)
	{
		this.bill=bill;
	}
	public Controller (JTextField idtofind,JTextArea foundcustomer,int type)
	{
		if (type==1)
		{
		this.idtofind=idtofind;
		this.foundcustomer=foundcustomer;
		}
		else
			if (type==2)
			{
				this.oid=idtofind;
				this.foundorder=foundcustomer;
			}
			else
				if (type==3)
				{
					this.did=idtofind;
					this.founddetail=foundcustomer;
				}
				else
					if(type==4)
					{
						this.nametoFind=idtofind;
						this.foundcustname=foundcustomer;
					}
					else
						if (type==5)
						{
							this.pid=idtofind;
							this.foundproduct=foundcustomer;
						}
						else
							if (type==6)
							{
								this.prodnametoFind=idtofind;
								this.foundprodname=foundcustomer;
							}
							else
								if(type==7)
								{
									this.idtobill=idtofind;
									this.bill=foundcustomer;
								}
	}
	public Controller(JTable custTable,int type)
	{
		if (type==1)
		{
		this.custTable=custTable;
		}
		else
			if (type==2)
				this.ordTable=custTable;
			else
				if (type==3)
					this.orddetailsTable=custTable;
				else
					if (type==4)
					{
						this.prodTable=custTable;
					}
	}
	public Controller(JTextField id,int type)
	{
		if (type==1)
			this.idtoDelete=id;
		else
			if (type==2)
				this.oidtoDelete=id;
			else
				if (type==3)
					this.didtoDelete=id;
				else
					if (type==4)
						this.pidtoDelete=id;
	}
	public Controller(JTextField amount,JTextField oid,JTextField pid,int type)
	{
		if (type==1)
		{
			this.amount=amount;
			this.oidtoInsert=oid;
			this.pidtoInsert=pid;
		}
		else
			if (type==2)
			{
				this.price=amount;
				this.quantity=oid;
				this.name=pid;
				
			}
	}
	/**
	 * finds a customer by id introduced in GUI
	 */
	public void findById()
	{
		try
		{
			int id=Integer.parseInt(idtofind.getText());
			Customer c=custbll.findCustomerbyId(id);
			foundcustomer.setText(c.toString());
		}
		catch (Exception e)
		{
			JOptionPane.showMessageDialog(null,"Id was not found or was not introduced as an integer");
		}
	}
	/**
	 * inserts a customer based on the text fields introduced by user
	 */
	public void insertCustomer()
	{
		try 
		{
			String name=nametoInsert.getText();
			String address=addresstoInsert.getText();
			String email=emailtoInsert.getText();
			int age=Integer.parseInt(agetoInsert.getText());
			if (age>18 && age<45)
			{
			Customer c=new Customer(name,address,email,age);
			int id=custbll.insertCustomer(c);
			if (id>-1)
				JOptionPane.showMessageDialog(null, "Customer was inserted successfully ");
			else
				JOptionPane.showMessageDialog(null, "Customer couldn't be inserted");
			}
			else
				JOptionPane.showMessageDialog(null, "Customer age is not between the right parameters");
		}
		catch (Exception e)
		{
			JOptionPane.showMessageDialog(null, "Data were not introduced correctly");
		}
	}
	/**
	 * Draws the customer table
	 */
	public void printCustomers()
	{
		custTable.setModel(custbll.fillCustomerData());
	}
	/**
	 * Deletes a customer based on the id introduced in the GUI
	 */
	public void deleteCustomer()
	{
		try
		{
			int id=Integer.parseInt(idtoDelete.getText());
			custbll.deleteCustomer(id);
			JOptionPane.showMessageDialog(null, "The deletion was performed succesfully");
		}
		catch (Exception e)
		{
			JOptionPane.showMessageDialog(null, "An error occured while trying to delete the customer");
		}
	}
	/**
	 * Finds a customer by the name introduced in the GUI
	 */
	public void findCustbyName()
	{
		try
		{
			String name=nametoFind.getText();
			Customer c=custbll.findCustomerbyName(name);
			foundcustname.setText(c.toString());
		}
		catch (Exception e)
		{
			JOptionPane.showMessageDialog(null,"Name was not found ");
		}
	}
	/**
	 * Finds an order by the ID introduced in the GUI 
	 */
	public void findorderByID()
	{
		try
		{
			int id=Integer.parseInt(oid.getText());
			Order c=ordbll.findOrderbyId(id);
			foundorder.setText(c.toString());
		}
		catch (Exception e)
		{
			JOptionPane.showMessageDialog(null,"Id was not found or was not introduced as an integer");
		}
	}
	/**
	 * Inserts an order based on the text fields introduced by the user
	 */
	public void insertOrder()
	{
		try 
		{
			int totPrice=Integer.parseInt(totPricetoInsert.getText());
			int cid=Integer.parseInt(cidtoInsert.getText());
			Order o=new Order(totPrice,cid);
			int id=ordbll.insertOrder(o);
			if (id>-1)
				JOptionPane.showMessageDialog(null, "Order was inserted successfully ");
			else
				JOptionPane.showMessageDialog(null, "Order couldn't be inserted");
		}
		catch (Exception e)
		{
			JOptionPane.showMessageDialog(null, "Data were not introduced correctly");
		}
	}
	/**
	 * Draws the order table
	 */
	public void printOrders()
	{
		ordTable.setModel(ordbll.fillOrderData());
	}
	/**
	 * Deletes an order based on the id introduced in the GUI
	 */
	public void deleteOrder()
	{
		try
		{
			int id=Integer.parseInt(oidtoDelete.getText());
			ordbll.deleteOrder(id);
			JOptionPane.showMessageDialog(null, "The deletion was performed succesfully");
		}
		catch (Exception e)
		{
			JOptionPane.showMessageDialog(null, "An error occured while trying to delete the customer");
		}
	}
	/**
	 * Find an order Detail by the id introduced in the GUI
	 */
	public void findDetailById()
	{
		try
		{
			int id=Integer.parseInt(did.getText());
			OrderDetails c=orddbll.findOrdDetailbyId(id);
			founddetail.setText(c.toString());
		}
		catch (Exception e)
		{
			JOptionPane.showMessageDialog(null,"Id was not found or was not introduced as an integer");
		}
	}
	/**
	 * Inserts an order detail based on the text fields introduced by the user
	 */
	public void insertOrderDetail()
	{
		try 
		{
			int am=Integer.parseInt(amount.getText());
			int oid=Integer.parseInt(oidtoInsert.getText());
			int pid=Integer.parseInt(pidtoInsert.getText());
			OrderDetails o=new OrderDetails (am,oid,pid);
			int id=orddbll.insertOrderDetails(o);
			if (id>-1)
				JOptionPane.showMessageDialog(null, "OrderDetail was inserted successfully ");
			else
				JOptionPane.showMessageDialog(null, "OrderDetail couldn't be inserted");
		}
		catch (Exception e)
		{
			JOptionPane.showMessageDialog(null, "Data were not introduced correctly");
		}
	}
	/**
	 * Draws the order details table in the GUI
	 */
	public void printOrderDetails()
	{
		orddetailsTable.setModel(orddbll.fillOrdDetailsData());
	}
	/**
	 * Deletes an order detail based on the id introduced in the GUI
	 */
	public void deleteOrderDetail()
	{
		try
		{
			int id=Integer.parseInt(didtoDelete.getText());
			orddbll.deleteOrderDetails(id);
			JOptionPane.showMessageDialog(null, "The deletion was performed succesfully");
		}
		catch (Exception e)
		{
			JOptionPane.showMessageDialog(null, "An error occured while trying to delete the customer");
		}
	}
	/**
	 * Finds a product based on the id introduced by the user
	 */
	public void findProductbyId()
	{
		try
		{
			int id=Integer.parseInt(pid.getText());
			Product c=prdbll.findProductbyId(id);
			foundproduct.setText(c.toString());
		}
		catch (Exception e)
		{
			JOptionPane.showMessageDialog(null,"Id was not found or was not introduced as an integer");
		}
	}
	/**
	 * Inserts a product based on the text fields introduced by the user
	 */
	public void insertProduct()
	{
		try 
		{
			int pr=Integer.parseInt(price.getText());
			int quant=Integer.parseInt(quantity.getText());
			String nume=name.getText();
			if (pr>0&&pr<20){
			Product c=new Product(pr,quant,nume);
			int id=prdbll.insertProduct(c);
			if (id>-1)
				JOptionPane.showMessageDialog(null, "Product was inserted successfully ");
			else
				JOptionPane.showMessageDialog(null, "Product couldn't be inserted");
			}
			else
				JOptionPane.showMessageDialog(null, "Product price is not between the specific ranges");
		}
		catch (Exception e)
		{
			JOptionPane.showMessageDialog(null, "Data were not introduced correctly");
		}
	}
	/**
	 * Draws the product table in the GUI
	 */
	public void printProducts()
	{
		prodTable.setModel(prdbll.fillProductData());
	}
	/**
	 * Deletes a product based on the id introduced in the GUI
	 */
	public void deleteProduct()
	{
		try
		{
			int id=Integer.parseInt(pidtoDelete.getText());
			prdbll.deleteProduct(id);
			JOptionPane.showMessageDialog(null, "The deletion was performed succesfully");
		}
		catch (Exception e)
		{
			JOptionPane.showMessageDialog(null, "An error occured while trying to delete the customer");
		}
	}
	/**
	 * Finds a product by the name introduced in the GUI
	 */
	public void findProdbyName()
	{
		try
		{
			String name=prodnametoFind.getText();
			Product c=prdbll.findProductbyName(name);
			foundprodname.setText(c.toString());
		}
		catch (Exception e)
		{
			JOptionPane.showMessageDialog(null,"Name was not found ");
		}
	}
	/**
	 * 
	 * @return a String of the resulted Bill computed based on the order id 
	 */
	public void makeBill()
	{
		ArrayList<Customer> allcustomers=new ArrayList<Customer>();
		ArrayList<Order> allorders=new ArrayList<Order>();
		ArrayList<OrderDetails> allorddetails=new ArrayList<OrderDetails>();
		ArrayList<Product> allproducts=new ArrayList<Product>();
		String result=new String();
		allcustomers=custbll.getallCustomers();
		allorders=ordbll.getallOrders();
		allorddetails=orddbll.getallOrderDetails();
		allproducts=prdbll.getallProducts();
		int oid=Integer.parseInt(idtobill.getText());
		int id=0;
		for (Order o : allorders)
		{
			if(o.getOid()==oid)
			{
				result+=" Order " +o.getOid() +" Total price "+o.getTotPrice()+" euros  Customer id "+ o.getCid() +"\n";
				id=o.getCid();
			}
		}
		for (Customer c: allcustomers)
		{
			if(c.getCid()==id)
			{
				result +="Customer name "+c.getName() + " Address " + c.getAddress() + " Age " + c.getAge()+"\n";
			}
		}
		int pid=0;
		for (OrderDetails ord: allorddetails)
		{
			if (ord.getOid()==oid)
			{
				result+=" Product id "+ord.getPid() + "Amount "+ ord.getAmount()+"\n";
				pid=ord.getPid();
				for (Product p: allproducts)
				{
					if (pid==p.getPid())
						result+="Id of product " +p.getPid()+ " Name " + p.getName()+ " Price for one is " + p.getPrice()+"euros \n";
				}
			}
		}
		bill.setText(result);
	}
	
}
