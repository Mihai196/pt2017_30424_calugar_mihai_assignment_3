package blogic;

import java.util.ArrayList;
import java.util.NoSuchElementException;

import javax.swing.table.DefaultTableModel;

import dao.CustomerDAO;
import dao.OrderDAO;
import model.Order;
import model.Order;

public class OrderBLL {
	/**
	 * 
	 * @param oid
	 * @return an object of type order if the id was found
	 */
	public Order findOrderbyId(int oid)
	{
		Order c=OrderDAO.findById(oid);
		if (c == null) {
			throw new NoSuchElementException("The Order with id =" + oid + " was not found!");
		}
		return c;
	}
	/**
	 * 
	 * @param c Order to be introduced
	 * @return id of the inserted Order
	 */
	public int insertOrder(Order c)
	{
		return OrderDAO.insert(c);
	}
	/**
	 * 
	 * @param pid delets on order by an id 
	 */
	public void deleteOrder(int pid)
	{
		OrderDAO.delete(pid);
	}
	/**
	 * 
	 * @return all list of orders from the order Table
	 */
	public ArrayList<Order> getallOrders()
	{
		ArrayList<Order> allorders=new ArrayList<Order>();
		allorders=OrderDAO.getAllOrders();
		return allorders;
	}
	/**
	 * 
	 * @return a Table model which draw the whole order table
	 */
	public DefaultTableModel fillOrderData()
	{
		DefaultTableModel tablemodel=new DefaultTableModel();
		tablemodel.addColumn("Oid");
		tablemodel.addColumn("totPrice");
		tablemodel.addColumn("Cid");
		ArrayList<Order> allorders=new ArrayList<Order>();
		allorders=OrderDAO.getAllOrders();
		for (Order c: allorders)
		{
			tablemodel.addRow(new Object[]{c.getOid(),c.getTotPrice(),c.getCid()});
		}
		return tablemodel;
	}

}
