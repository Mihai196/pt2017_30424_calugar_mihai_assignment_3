package blogic;

import java.util.ArrayList;
import java.util.NoSuchElementException;

import javax.swing.table.DefaultTableModel;

import dao.CustomerDAO;
import dao.OrderDetailsDAO;
import model.Customer;
import model.OrderDetails;


public class OrderDetailsBLL {
	
	/**
	 * 
	 * @param detailid
	 * @return an object of type OrderDetails if the id was found
	 */
	public OrderDetails findOrdDetailbyId(int detailid)
	{
		OrderDetails c=OrderDetailsDAO.findById(detailid);
		if (c == null) {
			throw new NoSuchElementException("The product with id =" + detailid + " was not found!");
		}
		return c;
	}
	/**
	 * 
	 * @param o OrderDetails to be inserted
	 * @return the id of the newly inserted object
	 */
	public int insertOrderDetails(OrderDetails o)
	{
		return OrderDetailsDAO.insert(o);
	}
	/**
	 * 
	 * @param detailid deletes the OrderDetails with the detailid
	 */
	public void deleteOrderDetails(int detailid)
	{
		OrderDetailsDAO.delete(detailid);
	}
	/**
	 * 
	 * @return returns a list of OrderDetails containing the whole OrderDetails table
	 */
	public ArrayList<OrderDetails> getallOrderDetails()
	{
		ArrayList<OrderDetails> allorddetails=new ArrayList<OrderDetails>();
		allorddetails=OrderDetailsDAO.getAllOrders();
		return allorddetails;
	}
	/**
	 * 
	 * @return a default table Model representing the drawing of the OrderDetails Table in the GUI
	 */
	public DefaultTableModel fillOrdDetailsData()
	{
		DefaultTableModel tablemodel=new DefaultTableModel();
		tablemodel.addColumn("Detailid");
		tablemodel.addColumn("Amount");
		tablemodel.addColumn("Oid");
		tablemodel.addColumn("Pid");
		ArrayList<OrderDetails> allordetails=new ArrayList<OrderDetails>();
		allordetails=OrderDetailsDAO.getAllOrders();
		for (OrderDetails ord: allordetails)
		{
			tablemodel.addRow(new Object[]{ord.getDetailid(),ord.getAmount(),ord.getOid(),ord.getPid()});
		}
		return tablemodel;
	}
}
