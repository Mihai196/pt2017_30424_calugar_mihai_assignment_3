package blogic;

import java.util.ArrayList;
import java.util.NoSuchElementException;

import javax.swing.table.DefaultTableModel;

import dao.CustomerDAO;
import model.Customer;

public class CustomerBLL {
	/**
	 * 
	 * @param cid 
	 * @return an object of type Customer if the id was found
	 */
	public Customer findCustomerbyId(int cid)
	{
		Customer c=CustomerDAO.findById(cid);
		if (c == null) {
			throw new NoSuchElementException("The customer with id =" + 1 + " was not found!");
		}
		return c;
	}
	/**
	 * 
	 * @param name
	 * @return object of type Customer if the name was found
	 */
	public Customer findCustomerbyName(String name)
	{
		Customer c=CustomerDAO.findByName(name);
		if (c == null) {
			throw new NoSuchElementException("The customer with name =" + name + " was not found!");
		}
		return c;
	}
	/**
	 * 
	 * @param c Customer to be inserted
	 * @return id of the newly inserted customer
	 */
	public int insertCustomer(Customer c)
	{
		return CustomerDAO.insert(c);
	}
	/**
	 * 
	 * @param cid deletes the customer with the mentioned id
	 */
	public void deleteCustomer(int cid)
	{
		CustomerDAO.delete(cid);
	}
	/**
	 * 
	 * @return a list of all customers from the customer table
	 */
	public ArrayList<Customer> getallCustomers()
	{
		ArrayList<Customer> allcustomers=new ArrayList<Customer>();
		allcustomers=CustomerDAO.getAllcustomers();
		return allcustomers;
	}
	/**
	 * 
	 * @return a default Table Mode representing the whole table
	 */
	public DefaultTableModel fillCustomerData()
	{
		DefaultTableModel tablemodel=new DefaultTableModel();
		tablemodel.addColumn("Cid");
		tablemodel.addColumn("Name");
		tablemodel.addColumn("Address");
		tablemodel.addColumn("Email");
		tablemodel.addColumn("Age");
		ArrayList<Customer> allcustomers=new ArrayList<Customer>();
		allcustomers=CustomerDAO.getAllcustomers();
		for (Customer c: allcustomers)
		{
			tablemodel.addRow(new Object[]{c.getCid(),c.getName(),c.getAddress(),c.getEmail(),c.getAge()});
		}
		return tablemodel;
	}
	
}
