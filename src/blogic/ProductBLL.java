package blogic;

import java.util.ArrayList;
import java.util.NoSuchElementException;

import javax.swing.table.DefaultTableModel;

import dao.CustomerDAO;
import dao.ProductDAO;
import model.Customer;
import model.Product;

public class ProductBLL {
	
	public Product findProductbyId(int pid)
	{
		Product c=ProductDAO.findById(pid);
		if (c == null) {
			throw new NoSuchElementException("The product with id =" + pid + " was not found!");
		}
		return c;
	}
	public Product findProductbyName(String name)
	{
		Product c=ProductDAO.findByName(name);
		if (c == null) {
			throw new NoSuchElementException("The product with name =" + name + " was not found!");
		}
		return c;
	}
	public int insertProduct(Product c)
	{
		return ProductDAO.insert(c);
	}
	public void deleteProduct(int pid)
	{
		ProductDAO.delete(pid);
	}
	public ArrayList<Product> getallProducts()
	{
		ArrayList<Product> allproducts=new ArrayList<Product>();
		allproducts=ProductDAO.getAllProducts();
		return allproducts;
	}
	public DefaultTableModel fillProductData()
	{
		DefaultTableModel tablemodel=new DefaultTableModel();
		tablemodel.addColumn("Pid");
		tablemodel.addColumn("Pame");
		tablemodel.addColumn("Quantity");
		tablemodel.addColumn("Name");
		ArrayList<Product> allproducts=new ArrayList<Product>();
		allproducts=ProductDAO.getAllProducts();
		for (Product c: allproducts)
		{
			tablemodel.addRow(new Object[]{c.getPid(),c.getPrice(),c.getQuantity(),c.getName()});
		}
		return tablemodel;
	}
	
	

}
