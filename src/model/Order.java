package model;

public class Order {
	private int oid;
	private int totPrice;
	private int cid;
	
	
	public Order(int oid, int totPrice, int cid) {
		this.oid = oid;
		this.totPrice = totPrice;
		this.cid = cid;
	}
	
	public Order(int totPrice, int cid) {
		this.totPrice = totPrice;
		this.cid = cid;
	}

	public int getOid() {
		return oid;
	}
	public void setOid(int oid) {
		this.oid = oid;
	}
	public int getTotPrice() {
		return totPrice;
	}
	public void setTotPrice(int totPrice) {
		this.totPrice = totPrice;
	}
	public int getCid() {
		return cid;
	}
	public void setCid(int cid) {
		this.cid = cid;
	}
	public String toString() {
		return "Order [id= " + oid + " ,total Price " + totPrice + " ,Customer Id "+cid
				+ "]";
	}
	
}
