package model;

public class OrderDetails {
	private int detailid;
	private int amount;
	private int oid;
	private int pid;
	public OrderDetails(int detailid, int amount, int oid, int pid) {
		super();
		this.detailid = detailid;
		this.amount = amount;
		this.oid = oid;
		this.pid = pid;
	}
	public OrderDetails(int amount, int oid, int pid) {
		super();
		this.amount = amount;
		this.oid = oid;
		this.pid = pid;
	}
	public int getDetailid() {
		return detailid;
	}
	public void setDetailid(int detailid) {
		this.detailid = detailid;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public int getOid() {
		return oid;
	}
	public void setOid(int oid) {
		this.oid = oid;
	}
	public int getPid() {
		return pid;
	}
	public void setPid(int pid) {
		this.pid = pid;
	}
	public String toString() {
		return "OrderDetails [id= " + detailid + " ,amount  " + amount + " , order id " + oid + " ,Product Id "+pid
				+ "]";
	}
}
