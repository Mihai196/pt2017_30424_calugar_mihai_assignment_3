package model;

public class Product {
	private int pid;
	private int price;
	private int quantity;
	private String name;
	public Product(int pid,int price,int quantity,String name)
	{
		this.pid=pid;
		this.price=price;
		this.quantity=quantity;
		this.name=name;
	}
	public Product(int price,int quantity,String name)
	{
		this.price=price;
		this.quantity=quantity;
		this.name=name;
	}
	public int getPid() {
		return pid;
	}
	public void setPid(int pid) {
		this.pid = pid;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "Product [id=" + pid + ", name=" + name + ", price=" + price + ", quantity=" + quantity
				+ "]";
	}
}
