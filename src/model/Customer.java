package model;


public class Customer {
	private int cid;
	private String name;
	private String address;
	private String email;
	private int age;

	
	public Customer(int cid,String name,String address,String email,int age)
	{
		this.cid=cid;
		this.name=name;
		this.address=address;
		this.email=email;
		this.age=age;
	}
	public Customer(String name,String address,String email,int age)
	{
		this.name=name;
		this.address=address;
		this.email=email;
		this.age=age;
	}
	public int getCid() {
		return cid;
	}
	public void setCid(int cid) {
		this.cid = cid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	@Override
	public String toString() {
		return "Customer [id=" + cid + ", name=" + name + ", address=" + address + ", email=" + email + ", age=" + age
				+ "]";
	}
	
	
}
